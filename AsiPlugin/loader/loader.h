#pragma once

#include <string>
#include <string_view>

/// Название проекта, которое было указано при разворачивание проекта из шаблона
extern const std::string_view PROJECT_NAME;
extern const std::wstring_view WPROJECT_NAME;

/**
 * @brief Выводит окно с сообщениемю
 * @detail Является оберткой над MessageBoxA, для более удобного вывода сообщений.
 * @param[in] text Текст сообщения.
 * @param[in] title Заголовок окна с сообщением.
 * @param[in] type Тип окна.
 * @return код завершения окна (нажатая кнопка).
 */
int MsgBox( const std::string &text, const std::string &title = PROJECT_NAME_C, int type = 0 );

/**
 * @brief Выводит окно с сообщениемю
 * @detail Является оберткой над MessageBoxA, для более удобного вывода сообщений.
 * @param[in] text Текст сообщения.
 * @param[in] title Заголовок окна с сообщением.
 * @param[in] type Тип окна.
 * @return код завершения окна (нажатая кнопка).
 */
int MsgBox( const std::wstring &text, const std::wstring &title = WPROJECT_NAME_C, int type = 0 );
