#include "loader.h"
#include "../main.h"

#include <asm.h>
#include <mem.h>

#include <cstring>

#if __has_include( <windows.h>)
#	include <windows.h>
#endif

const std::string_view PROJECT_NAME = PROJECT_NAME_C;
const std::wstring_view WPROJECT_NAME = WPROJECT_NAME_C;

[[maybe_unused]] class AsiPluginLoader {
	void *hook = nullptr;
	void *mainloopCaller = nullptr;

public:
	AsiPluginLoader() noexcept {
		hook = llmo::mem::allocate();
		auto hook_code = hook;
		auto storage = (void *)( (std::uintptr_t)hook_code + ( llmo::mem::pageSize() / 2 ) );

		auto exe = llmo::mem::lib::find( "SanAndreas.exe" );
		auto hook_addr = (void *)( exe + 0x110F2DC );
		auto hook_length = 14;

		auto len = llmo::assembler::writeCpuToHeap( hook_code, storage );
		hook_code = (void *)( (std::uintptr_t)hook_code + len );
		len = llmo::assembler::branch::makeDirectCall( hook_code, (void *)&mainloop, llmo::platforms::cpu::registers::RAX );
		if ( len ) mainloopCaller = hook_code;
		hook_code = (void *)( (std::uintptr_t)hook_code + len );
		len = llmo::assembler::readCpuFromHeap( hook_code, storage );
		hook_code = (void *)( (std::uintptr_t)hook_code + len + 1 ); // crutch + 1. Need find bug in llmo::assembler!
		std::memcpy( hook_code, hook_addr, hook_length );

		auto prot = llmo::mem::prot::get( hook_addr, hook_length );
		llmo::mem::prot::set( hook_addr, hook_length );
		llmo::assembler::makeNOP( hook_addr, hook_length );
		llmo::assembler::branch::makeDirectJmp( hook_addr, hook, llmo::platforms::cpu::registers::RDI );
		llmo::mem::prot::set( hook_addr, hook_length, prot );
	}
	~AsiPluginLoader() noexcept {
		if ( !mainloopCaller ) return;
		llmo::assembler::makeNOP( mainloopCaller, llmo::assembler::branch::consts::directCallLen );
	}
} g_loader;

int MsgBox( const std::string &text, const std::string &title, int type ) {
#if __has_include( <windows.h>)
	return MessageBoxA( nullptr, text.c_str(), title.c_str(), type );
#else
	return 0
#endif
}

int MsgBox( const std::wstring &text, const std::wstring &title, int type ) {
#if __has_include( <windows.h>)
	return MessageBoxW( nullptr, text.c_str(), title.c_str(), type );
#else
	return 0
#endif
}
